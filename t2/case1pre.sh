#!/bin/bash
#SBATCH --job-name=pre
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o case1pre_%A_%a.out
#SBATCH -e case1pre_%A_%a.err

prefix=(ml mc mp mv)

file=(${prefix[$SLURM_ARRAY_TASK_ID]}m?.vcf)
for f in ${file[@]}
do
#### Use the command below if don't filter for single copy regions ####
#        awk '$1!~/^#/{print $1,$2,$4,$5}' OFS='\t' $f > $f\.tmp
#######################################################################

#### Use the command below if filter for single copy regions ####
	awk '!/^#/{print $1,$2,$2,$4,$5}' OFS='\t' $f |bedtools intersect -a - -b ${prefix[$SLURM_ARRAY_TASK_ID]}.flt.bed -wa|awk '{print $1,$2,$4,$5}' OFS='\t' > $f\.tmp
#################################################################
done

cat ${prefix[$SLURM_ARRAY_TASK_ID]}m?.vcf.tmp |sort -k1,1 -k2,2n|uniq -c|awk '$1==3{print $2,$3,$4,$5}' OFS='\t' > ${prefix[$SLURM_ARRAY_TASK_ID]}.specific.vcf
rm ${prefix[$SLURM_ARRAY_TASK_ID]}m?.vcf.tmp



