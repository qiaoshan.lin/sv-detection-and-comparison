#!/bin/bash
#SBATCH --job-name=case2pre3
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

ref=(ml ml ml mc mc mc mp mp mp mv mv mv)
obj1=(mp mc mc mv ml ml mv mv ml mp mp mc)
obj2=(mv mv mp mp mv mp mc ml mc mc ml ml)
target=(mpmv mcmv mcmp mvmp mlmv mlmp mvmc mvml mlmc mpmc mpml mcml)
test1=(mlmp mlmv mlmc mcmp mcmv mcml mpmc mpmv mpml mvmc mvmp mvml)
test2=(mlmv mlmc mlmp mcmv mcml mcmp mpmv mpml mpmc mvmp mvml mvmc)
test3=(mlmc mlmp mlmv mcml mcmp mcmv mpml mpmc mpmv mvml mvmc mvmp)

cat ${test1[$SLURM_ARRAY_TASK_ID]}.vcf.tmp ${test2[$SLURM_ARRAY_TASK_ID]}.vcf.tmp |sort -k1,1 -k2,2n|uniq -c|awk '$1==2{print $2,$3,$4,$5}' OFS='\t' > ${target[$SLURM_ARRAY_TASK_ID]}.tmp
cat ${target[$SLURM_ARRAY_TASK_ID]}.tmp ${test3[$SLURM_ARRAY_TASK_ID]}.vcf.tmp |sort -k1,1 -k2,2n|uniq -c|awk '$1==2{print $2,$3,$4,$5}' OFS='\t' |cat - ${target[$SLURM_ARRAY_TASK_ID]}.tmp|sort -k1,1 -k2,2n|uniq -c|awk '$1==1{print $2,$3,$4,$5}' OFS='\t' > ${target[$SLURM_ARRAY_TASK_ID]}.${ref[$SLURM_ARRAY_TASK_ID]}.share.vcf
rm ${target[$SLURM_ARRAY_TASK_ID]}.tmp

#### find corresponding query snps based on ref snps

line=()
while read line; do line+=("$line"); done < ${target[$SLURM_ARRAY_TASK_ID]}.${ref[$SLURM_ARRAY_TASK_ID]}.share.vcf

for l in ${line[@]}
do
	mydiff=$(echo "$l" | awk '{print length($4)-length($3)}')
	if (( $mydiff==0 ));then
		mypos=$(echo "$l" | perl -lane 'print "$F[0]\t$F[1]\t"')
		grep -P -m 1 "$mypos" ${ref[$SLURM_ARRAY_TASK_ID]}${obj1[$SLURM_ARRAY_TASK_ID]}.snps.tmp | cut -f5-8 >> ${obj1[$SLURM_ARRAY_TASK_ID]}.${target[$SLURM_ARRAY_TASK_ID]}.${ref[$SLURM_ARRAY_TASK_ID]}.share.vcf
		grep -P -m 1 "$mypos" ${ref[$SLURM_ARRAY_TASK_ID]}${obj2[$SLURM_ARRAY_TASK_ID]}.snps.tmp | cut -f5-8 >> ${obj2[$SLURM_ARRAY_TASK_ID]}.${target[$SLURM_ARRAY_TASK_ID]}.${ref[$SLURM_ARRAY_TASK_ID]}.share.vcf
	fi
done


