library(ggplot2)
library(gridExtra)

dir <- "/Volumes/GoogleDrive/My\ Drive/Qiaoshan/Genome_assembly_annotation/SNPvariance/"
setwd(dir)

myTheme1 = theme(
  plot.tag = element_text(size = rel(1.3)),
  panel.grid = element_blank(),
  axis.line = element_line(colour = "black"), 
  panel.background = element_blank(),
  axis.text = element_text(size = rel(1.2)), 
  axis.text.y = element_text(angle = 45, hjust = 0.5),
  axis.title = element_text(size = rel(1.2)),
  legend.text = element_text(size = rel(1.2)), 
  legend.title = element_text(size = rel(1.3))
)

#######################################
# count by features across main types #
#######################################
main <- read.table(file = "counts_by_features_across_main_types.txt", sep = "\t", header = TRUE)
main$species <- factor(main$species, levels = c("LF10","CE10","Mpar","MvBL"))
main$type <- factor(main$type, levels = c("transition","transversion","insertion","deletion"), labels = c("Transition","Transversion","Insertion","Deletion"))
main$feature <- factor(main$feature, levels = c("exon","intron","intergenic"),labels = c("Exon","Intron","Others"))

mainFig <- ggplot(data = main, aes(species, count, fill=feature))+
  geom_bar(stat = "identity",position = "fill")+
  facet_grid(.~type)+
  scale_fill_manual("Features", values=c("#c20078","#ffcfdc","#b7c9e2"))+
  ylab("")+
  xlab("Species")+
  myTheme1+
  theme(panel.grid.major = element_line(colour = "grey"), panel.border = element_rect(colour = "black", fill = NA))+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.6))+
  theme(strip.text = element_text(size = rel(1.3)))+
  scale_y_continuous(labels = scales::percent)
mainFig

#########################################
# count by features across indel length #
#########################################
indel <- read.table(file = "counts_by_features_across_indel_length.txt", sep = "\t", header = TRUE)
indel$species <- factor(indel$species, levels = c("LF10","CE10","Mpar","MvBL"))
indel$type <- factor(indel$type, levels = c("insertion","deletion"), labels = c("Insertion","Deletion"))
indel$feature <- factor(indel$feature, levels = c("exon","intron","intergenic"),labels = c("Exon","Intron","Others"))

indelFig <- ggplot(data = indel)+
  geom_bar(aes(length,count,fill=feature), stat = "identity", position = "fill")+
  facet_grid(type~species, scales="free", space="free", switch = "y")+
  scale_fill_manual("Features", values=c("#c20078","#ffcfdc","#b7c9e2"))+
  ylab("")+
  myTheme1+
  theme(panel.grid.major = element_line(colour = "grey"), panel.border = element_rect(colour = "black", fill = NA))+
  theme(axis.text.y = element_blank(),axis.line.y = element_blank(),axis.ticks.y = element_blank())+
  scale_x_continuous(name = "Length", limits = c(0,21), breaks = seq(3,20,3))+
  theme(strip.text = element_text(size = rel(1.3)))
indelFig

#################
# merge figures #
#################
p1<-mainFig+labs(tag="A")
p2<-indelFig+labs(tag="B")
grid.arrange(p1,p2,
             widths = c(1,1),
             layout_matrix = rbind(c(1,1),
                                   c(1,1),
                                   c(2,2),
                                   c(2,2),
                                   c(2,2))
)
dev.copy(png, "Figure1C.png", width=1200,height=1200,res=135)
dev.off()
