#!/bin/bash
#SBATCH --job-name=case2pre2
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

target=(mlmc mlmp mlmv mcml mcmp mcmv mpml mpmc mpmv mvml mvmc mvmp)
awk '{print $13,$1,$2,$3,$14,$4,$3,$2}' OFS='\t' ${target[$SLURM_ARRAY_TASK_ID]}.snps > ${target[$SLURM_ARRAY_TASK_ID]}.snps.tmp


