#!/bin/bash
#SBATCH --job-name=case1B
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o case1B_%j.out
#SBATCH -e case1B_%j.err

module load bedtools

prefix=(ml mc mp mv)
label=(LF10 CE10 Mpar MvBL)
lengthfiles=(~/resource/LF10/LF10g_v2.0_nucleus.length ~/resource/CE10/CE10g_v2.0_nucleus.length ~/resource/Mpar/Mparg_v2.0_nucleus.length ~/resource/MvBL/MvBLg_v2.0_nucleus.length)

# count by windows across main types
windowSize=200000
for s in ${!prefix[@]}
do
	bedtools makewindows -g ${lengthfiles[$s]} -w $windowSize > ${prefix[$s]}.genome.bed
done

fileName=counts_by_windows_across_main_types.txt
perl -e 'print "type\tspecies\tchromosome\tstart\tend\tcount\n"' > $fileName
for s in ${!prefix[@]}
do
        cat ${prefix[$s]}.specific.vcf | perl -lane '{$tmp=$F[2]."_".$F[3]; print "$F[0]\t$F[1]\t$F[1]" if $tmp =~ /^A_T$|^A_C$|^G_T$|^G_C$|^T_A$|^T_G$|^C_A$|^C_G$/}' > tmp.bed
        bedtools coverage -a ${prefix[$s]}.genome.bed -b tmp.bed -counts | perl -slane 'print "transversion\t$label\t$_"' -- -label=${label[$s]} |awk '$3~/chr/{print}' >> $fileName

        cat ${prefix[$s]}.specific.vcf | perl -lane '{$tmp=$F[2]."_".$F[3]; print "$F[0]\t$F[1]\t$F[1]" if $tmp =~ /^G_A$|^A_G$|^C_T$|^T_C$/}' > tmp.bed
        bedtools coverage -a ${prefix[$s]}.genome.bed -b tmp.bed -counts | perl -slane 'print "transition\t$label\t$_"' -- -label=${label[$s]} |awk '$3~/chr/{print}' >> $fileName

	cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]" if length($F[2])>length($F[3])' > tmp.bed
        bedtools coverage -a ${prefix[$s]}.genome.bed -b tmp.bed -counts | perl -slane 'print "insertion\t$label\t$_"' -- -label=${label[$s]} |awk '$3~/chr/{print}' >> $fileName

        cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]" if length($F[2])<length($F[3])' > tmp.bed
        bedtools coverage -a ${prefix[$s]}.genome.bed -b tmp.bed -counts | perl -slane 'print "deletion\t$label\t$_"' -- -label=${label[$s]} |awk '$3~/chr/{print}' >> $fileName
	
	sed -i "s/${label[$s]}\_//" $fileName
done
rm tmp.bed

# count by windows across different indel length
fileName=counts_by_windows_across_indel_length.txt
perl -e 'print "type\tlength\tspecies\tchromosome\tstart\tend\tcount\n"' > $fileName
for s in ${!prefix[@]}
do
        cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]\t",length($F[2])-length($F[3]) if length($F[2])>length($F[3])' > tmp.bed
        cut -f4 tmp.bed | sort -n |uniq > length.tmp
        len=()
        while read line; do len+=("$line"); done < length.tmp
        for l in ${len[@]}
        do
                cat tmp.bed | perl -slane 'if($F[3]==$len){print}' -- -len=$l > mytmp.bed
                bedtools coverage -a ${prefix[$s]}.genome.bed -b mytmp.bed -counts | perl -slane 'print "insertion\t$len\t$label\t$_"' -- -label=${label[$s]} -len=$l |awk '$4~/chr/{print}' >> $fileName
        done
        rm mytmp.bed length.tmp tmp.bed

        cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]\t",length($F[3])-length($F[2]) if length($F[2])<length($F[3])' > tmp.bed
        cut -f4 tmp.bed | sort -n |uniq > length.tmp
        len=()
        while read line; do len+=("$line"); done < length.tmp
        for l in ${len[@]}
        do
                cat tmp.bed | perl -slane 'if($F[3]==$len){print}' -- -len=$l > mytmp.bed
                bedtools coverage -a ${prefix[$s]}.genome.bed -b mytmp.bed -counts | perl -slane 'print "deletion\t$len\t$label\t$_"' -- -label=${label[$s]} -len=$l |awk '$4~/chr/{print}' >> $fileName
        done
        rm mytmp.bed length.tmp tmp.bed
	sed -i "s/${label[$s]}\_//" $fileName
done
awk '{if($1~/deletion/){print $1,$2,$3,$4,$5,$6,"-"$7}else{print}}' OFS='\t' $fileName > tmp
mv tmp $fileName 

# mummer aligned regions
fileName=mummer_align_regions.txt
perl -e 'print "species\tchromosome\tstart\tend\ty\n"' > $fileName
ypos=("-90" "-40" "-50" "-80")
for s in ${!prefix[@]}
do
#### Use the 3 command lines below if don't filter for single copy regions ####
#	cat ${prefix[$s]}m?.coords | awk '{print $8,$1,$2}' OFS='\t' | sort -k1,1 -k2,2n > tmp.bed
#	bedtools merge -i tmp.bed | perl -slane 'print "$label\t$_\t$ypos"' -- -label=${label[$s]} -ypos=${ypos[$s]}|sed "s/${label[$s]}\_//"|awk '$2~/chr/{print}' >> $fileName
#	rm tmp.bed
###############################################################################

#### Use the command below if filter for single copy regions ####
	cat ${prefix[$s]}.flt.bed | perl -slane 'print "$label\t$_\t$ypos"' -- -label=${label[$s]} -ypos=${ypos[$s]}|sed "s/${label[$s]}\_//"|awk '$2~/chr/{print}' >> $fileName
#################################################################
done
rm *.genome.bed


