#!/bin/bash
#SBATCH --job-name=case2pre1
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bedtools 

prefix=(ml mc mp mv)

file=(${prefix[$SLURM_ARRAY_TASK_ID]}m?.vcf)
for f in ${file[@]}
do
	awk '!/^#/{print $1,$2,$2,$4,$5}' OFS='\t' $f |bedtools intersect -a - -b ${prefix[$SLURM_ARRAY_TASK_ID]}.flt.bed -wa|awk '{print $1,$2,$4,$5}' OFS='\t' > $f\.tmp
done



