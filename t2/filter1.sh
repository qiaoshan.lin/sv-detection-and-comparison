#!/bin/bash
#SBATCH --job-name=filter1
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

prefix=(mlmc mlmp mlmv mcmp mcmv mpmv mcml mpml mvml mpmc mvmc mvmp)

module load bedtools 

awk '{print $8,$1,$2,$9,$3,$4}' OFS='\t' ${prefix[$SLURM_ARRAY_TASK_ID]}.coords > ${prefix[$SLURM_ARRAY_TASK_ID]}.bed

bedtools merge -i ${prefix[$SLURM_ARRAY_TASK_ID]}.bed -c 1,4,5,6 -o count,collapse,collapse,collapse|awk '$4==1{if($6<=$7){print $5,$6,$7,$1,$2,$3}else{print $5,$7,$6,$1,$2,$3}}' OFS='\t'|sort -k1,1 -k2,2n | bedtools merge -i - -c 1,4,5,6 -o count,collapse,collapse,collapse|awk '$4==1{print $5,$6,$7,$1,$2,$3}' OFS='\t'|sort -k1,1 -k2,2n > ${prefix[$SLURM_ARRAY_TASK_ID]}.one-to-one.bed

m1=`echo ${prefix[$SLURM_ARRAY_TASK_ID]}|perl -lane '/(m.?)(m.?)/;print $1'`
m2=`echo ${prefix[$SLURM_ARRAY_TASK_ID]}|perl -lane '/(m.?)(m.?)/;print $2'`

cut -f1-3 ${prefix[$SLURM_ARRAY_TASK_ID]}.one-to-one.bed > $m1\.${prefix[$SLURM_ARRAY_TASK_ID]}.one-to-one.bed
cut -f4-6 ${prefix[$SLURM_ARRAY_TASK_ID]}.one-to-one.bed > $m2\.${prefix[$SLURM_ARRAY_TASK_ID]}.one-to-one.bed

rm ${prefix[$SLURM_ARRAY_TASK_ID]}.one-to-one.bed ${prefix[$SLURM_ARRAY_TASK_ID]}.bed


