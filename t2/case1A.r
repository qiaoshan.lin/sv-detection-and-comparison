library(ggplot2)
library(export)
library(gridExtra)
library(Rmisc)
library(scales)
dir <- "/Volumes/GoogleDrive/My\ Drive/Qiaoshan/Genome_assembly_annotation/SNPvariance/"
setwd(dir)

myTheme1 = theme(
  plot.tag = element_text(size = rel(1.3)),
  panel.grid = element_blank(),
  axis.line = element_line(colour = "black"), 
  panel.background = element_blank(),
  axis.text = element_text(size = rel(1.2)), 
  axis.text.y = element_text(angle = 45, hjust = 0.5),
  axis.title = element_text(size = rel(1.2)),
  legend.text = element_text(size = rel(1.2)), 
  legend.title = element_text(size = rel(1.3))
)

#############################################
# overall mutation counts across main types #
#############################################
overallmain <- read.table(file = "overall_counts_across_main_types.txt", sep = "\t", header = TRUE)
overallmain$species <- factor(overallmain$species, levels = c("LF10","CE10","Mpar","MvBL"))
overallmain$type <- factor(overallmain$type, levels = c("transition","transversion","insertion","deletion"), labels = c("Transition","Transversion","Insertion","Deletion"))

overallmainFig <-ggplot(data = overallmain, aes(species, count, fill=type))+
  geom_bar(stat = "identity",position = "dodge")+
  scale_fill_manual("Mutation types", values=c("#ff796c","#c20078","#ffcfdc","#ff474c"))+
  ylab("Count")+
  xlab("Species")+
  scale_y_continuous(labels = scales::comma)+
  myTheme1
overallmainFig
#ggsave("overall_counts_across_main_types_dodge_group_by_species.png")

#########################
# point mutation counts #
#########################
pointMut <- read.table(file = "overall_counts_across_point_mutation_types.txt", sep = "\t", header = TRUE)
pointMut$species <- factor(pointMut$species, levels = c("LF10","CE10","Mpar","MvBL"))
pointMut$type <- factor(pointMut$type, levels = c("AG transition","TC transition","transversion"), labels = c("AG transition","TC transition","Transversion"))

pointMutFig <-ggplot(data = pointMut, aes(species, count, fill=type))+
  geom_bar(stat = "identity",position = "dodge")+
  scale_fill_manual("Mutation types", values=c("#fffd01","#ff796c","#c20078"))+
  ylab("Count")+
  xlab("Species")+
  labs(tag="A")+
  scale_y_continuous(labels = scales::comma)+
  myTheme1
pointMutFig

#############################
# indel length distribution #
#############################
indellen <- read.table(file = "indel_length_distribution.txt", sep = "\t", header = TRUE)
indellen$species <- factor(indellen$species, levels = c("LF10","CE10","Mpar","MvBL"))
indellen$type <- factor(indellen$type, levels = c("insertion","deletion"), labels = c("Insertion","Deletion"))
anno <- data.frame(lab = c("Insertion", "Deletion"), type = c("Insertion", "Deletion"))

indellenFig <-ggplot(data = indellen, aes(length, count))+
  geom_line(aes(color=species), stat = "identity", size = 2)+
  facet_grid(.~type)+
  scale_color_manual("Species", values=c("#FF9519","#C06DEB","#5DC500","#41A3F0"))+
  ylab("Count")+
  xlab("Length")+
  labs(tag="B")+
  scale_y_continuous(labels = scales::comma)+
  scale_x_continuous(trans = log10_trans(),
                     breaks = trans_breaks("log10", function(x) 10^x),
                     labels = trans_format("log10", math_format(10^.x))
                     )+
  myTheme1+
  theme(panel.grid.major.y = element_line(color = "gray",linetype = 3))+
  theme(strip.text.x = element_blank(), strip.background = element_blank())+
  theme(legend.key = element_blank())+
#### Use the 2 lines below if not filtered for single copy reigons ####
#  annotate("rect", xmin = 9, xmax = 45, ymin = 33000, ymax = 37000,alpha = 1,fill="white",color="black")+
#  geom_text(data=anno, aes(x = 20,  y = 35000, label = lab), size=5)
#### Use the 2 lines below if filtered for single copy regions ####
  annotate("rect", xmin = 9, xmax = 45, ymin = 9000, ymax = 11000,alpha = 1,fill="white",color="black")+
  geom_text(data=anno, aes(x = 20,  y = 10000, label = lab), size=5)
indellenFig

# grid.arrange(pointMutFig, indellenFig, 
#              widths = c(3,5),
#              nrow = 1)
# dev.copy(png, "point_mutation_counts_and_indel_length_distribution.png", width=1800,height=600,res=135)
# dev.off()

#################
# merge figures #
#################
p1<-overallmainFig+labs(tag="A")
p2<-pointMutFig+labs(tag="B")
p3<-indellenFig+labs(tag="C")
grid.arrange(p1,p2,p3,
             widths = c(1,1),
             layout_matrix = rbind(c(1,2),
                                   c(3,3))
)
dev.copy(png, "Figure1A.png", width=1500,height=900,res=135)
dev.off()
