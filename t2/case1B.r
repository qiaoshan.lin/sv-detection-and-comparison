library(ggplot2)
library(export)
library(gridExtra)
library(Rmisc)
library(scales)
dir <- "/Volumes/GoogleDrive/My\ Drive/Qiaoshan/Genome_assembly_annotation/SNPvariance/"
setwd(dir)

myTheme1 = theme(
  plot.tag = element_text(size = rel(1.3)),
  panel.grid = element_blank(),
  axis.line = element_line(colour = "black"), 
  panel.background = element_blank(),
  axis.text = element_text(size = rel(1.2)), 
  axis.text.y = element_text(angle = 45, hjust = 0.5),
  axis.title = element_text(size = rel(1.2)),
  legend.text = element_text(size = rel(1.2)), 
  legend.title = element_text(size = rel(1.3))
)

mummerAlign<-read.table(file = "mummer_align_regions.txt", header = TRUE, sep = '\t')
mummerAlign$chromosome <- factor(mummerAlign$chromosome, levels = c("chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8"), labels = c("Chromosome 1","Chromosome 2","Chromosome 3","Chromosome 4","Chromosome 5","Chromosome 6","Chromosome 7","Chromosome 8"))

#######################################
# counts by windows across main types #
#######################################
windowCountMain <- read.table(file = "counts_by_windows_across_main_types.txt", sep = "\t", header = TRUE)
windowCountMain$species <- factor(windowCountMain$species, levels = c("LF10","CE10","Mpar","MvBL"))
windowCountMain$type <- factor(windowCountMain$type, levels = c("transition","transversion","insertion","deletion"), labels = c("Transition","Transversion","Insertion","Deletion"))
windowCountMain$chromosome <- factor(windowCountMain$chromosome, levels = c("chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8"), labels = c("Chromosome 1","Chromosome 2","Chromosome 3","Chromosome 4","Chromosome 5","Chromosome 6","Chromosome 7","Chromosome 8"))

windowCountMainFig <-ggplot()+
  geom_line(data = windowCountMain, aes(start, count, color=type), stat = "identity",size=0.5)+
  facet_grid(species~chromosome, scales="free", space="free", switch = "both")+
  scale_color_manual("Mutation types", values=c("#ff796c","#c20078","#040273","#95d0fc"))+
  myTheme1+
  theme(strip.background = element_blank(), strip.text = element_text(size = rel(1.2)), strip.text.y = element_text(angle = 180), strip.placement = "outside")+
  theme(legend.key = element_blank())+
  scale_x_continuous(name="", labels=function(x)x/1000000, breaks=seq(0,80000000,10000000))+
### Use the 2 lines below if not filtered for single copy regions ###
#  scale_y_continuous(name="Count", breaks = seq(0,1000,200))+
#  geom_segment(data = mummerAlign, aes(x=start,y=-40,xend=end,yend=-40), size=2, color="black")
### Use the 2 lines below if filtered for single copy regions ###
  scale_y_continuous(name="Count", breaks = seq(0,900,200))+
  geom_segment(data = mummerAlign, aes(x=start-1000,y=-20,xend=end+1000,yend=-20), size=2, color="black")
windowCountMainFig

#ggsave("counts_by_windows_across_main_types.png",width = 20,height = 12)

###################################################
# counts by windows across different indel length #
###################################################
windowCountIndel <- read.table(file = "counts_by_windows_across_indel_length.txt", sep = "\t", header = TRUE)
windowCountIndel$species <- factor(windowCountIndel$species, levels = c("LF10","CE10","Mpar","MvBL"))
windowCountIndel$type <- factor(windowCountIndel$type, levels = c("insertion","deletion"), labels = c("Insertion","Deletion"))
windowCountIndel$chromosome <- factor(windowCountIndel$chromosome, levels = c("chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8"), labels = c("Chromosome 1","Chromosome 2","Chromosome 3","Chromosome 4","Chromosome 5","Chromosome 6","Chromosome 7","Chromosome 8"))

windowCountIndelFig <-ggplot()+
  geom_bar(data = windowCountIndel, aes(start, count, fill=length), stat = "identity", position = "stack")+
  facet_grid(species~chromosome, scales="free", space="free", switch = "both")+
  scale_fill_gradient("Indel length (bp)", low = "blue", high = "red")+
  myTheme1+
  theme(strip.background = element_blank(), strip.text = element_text(size = rel(1.2)), strip.text.y = element_text(angle = 180), strip.placement = "outside")+
  scale_x_continuous(name="", labels=function(x)x/1000000, breaks=seq(0,80000000,10000000))+
### Use the 2 lines below if not filtered for single copy regions ###
#  scale_y_continuous(name="Count", breaks = seq(-120,120,40))+
#  geom_segment(data = mummerAlign, aes(x=start,y=y,xend=end,yend=y), size=2, color="black")+
### Use the 2 lines below if filtered for single copy regions ###
  scale_y_continuous(name="Count", breaks = seq(-100,100,20))+
  geom_segment(data = mummerAlign, aes(x=start-1000,y=y,xend=end+1000,yend=y), size=2, color="black")+
  geom_hline(yintercept = 0)
windowCountIndelFig

#ggsave("counts_by_windows_indel_length_distribution.png",width = 20,height = 12)

#################
# merge figures #
#################
p1<-windowCountMainFig+labs(tag="A")
p2<-windowCountIndelFig+labs(tag="B")
grid.arrange(p1,p2,
             nrow=2)
dev.copy(png, "Figure1B.png", width=5400, height=4200,res=300)
dev.off()

