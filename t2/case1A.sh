#!/bin/bash
#SBATCH --job-name=case1A
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o case1A_%j.out
#SBATCH -e case1A_%j.err

prefix=(ml mc mp mv)
label=(LF10 CE10 Mpar MvBL)

# Overall counts
fileName=overall_counts.txt
perl -e 'print "species\tcount\n"' > $fileName
for s in ${!prefix[@]}
do
	cat ${prefix[$s]}.specific.vcf | perl -slane 'END{print "$label\t$."}' -- -label=${label[$s]} >> $fileName
done

# Overall counts across main types
fileName=overall_counts_across_main_types.txt
perl -e 'print "type\tspecies\tcount\n"' > $fileName
for s in ${!prefix[@]}
do
        cat ${prefix[$s]}.specific.vcf | perl -slane '{$tmp=$F[2]."_".$F[3]; $sum++ if $tmp =~ /^A_T$|^A_C$|^G_T$|^G_C$|^T_A$|^T_G$|^C_A$|^C_G$/}END{ print "transversion\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
	cat ${prefix[$s]}.specific.vcf | perl -slane '{$tmp=$F[2]."_".$F[3]; $sum++ if $tmp =~ /^G_A$|^A_G$|^C_T$|^T_C$/}END{ print "transition\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
	cat ${prefix[$s]}.specific.vcf | perl -slane '{$sum++ if length($F[2])>length($F[3])}END{ print "insertion\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
	cat ${prefix[$s]}.specific.vcf | perl -slane '{$sum++ if length($F[2])<length($F[3])}END{ print "deletion\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
done

# Overall counts for point mutations
fileName=overall_counts_across_point_mutation_types.txt
perl -e 'print "type\tspecies\tcount\n"' > $fileName
for s in ${!prefix[@]}
do 
	cat ${prefix[$s]}.specific.vcf | perl -slane '{$tmp=$F[2]."_".$F[3]; $sum++ if $tmp =~ /^A_T$|^A_C$|^G_T$|^G_C$|^T_A$|^T_G$|^C_A$|^C_G$/}END{ print "transversion\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
	cat ${prefix[$s]}.specific.vcf | perl -slane '{$tmp=$F[2]."_".$F[3]; $sum++ if $tmp =~ /^G_A$|^A_G$/}END{ print "AG transition\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
	cat ${prefix[$s]}.specific.vcf | perl -slane '{$tmp=$F[2]."_".$F[3]; $sum++ if $tmp =~ /^C_T$|^T_C$/}END{ print "TC transition\t$label\t$sum" }' -- -label=${label[$s]} >> $fileName
done

# Indel length distribution
fileName=indel_length_distribution.txt
perl -e 'print "species\tlength\tcount\ttype\n"' > $fileName
prefix=(ml mc mp mv)
label=(LF10 CE10 Mpar MvBL)
for s in ${!prefix[@]}
do
	cat ${prefix[$s]}.specific.vcf |awk 'length($3)>length($4){print length($3)-length($4)}'|sort -n|uniq -c|awk -v species="${label[$s]}" '{print species"\t"$2"\t"$1"\tinsertion"}' >> $fileName
	cat ${prefix[$s]}.specific.vcf |awk 'length($3)<length($4){print length($4)-length($3)}'|sort -n|uniq -c|awk -v species="${label[$s]}" '{print species"\t"$2"\t"$1"\tdeletion"}' >> $fileName
done



