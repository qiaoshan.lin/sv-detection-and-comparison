#!/bin/bash
#SBATCH --job-name=case1C
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o case1C_%j.out
#SBATCH -e case1C_%j.err

module load bedtools

prefix=(ml mc mp mv)
label=(LF10 CE10 Mpar MvBL)

# prepare features bed files
gtflocation=(~/resource/LF10/LF10g_v2.0.gtf ~/resource/CE10/CE10g_v2.0.gtf ~/resource/Mpar/Mparg_v2.0.gtf ~/resource/MvBL/MvBLg_v2.0.gtf)
lenlocation=(~/resource/LF10/LF10g_v2.0_nucleus.length ~/resource/CE10/CE10g_v2.0_nucleus.length ~/resource/Mpar/Mparg_v2.0_nucleus.length ~/resource/MvBL/MvBLg_v2.0_nucleus.length)
#maskout=(/home/CAM/qlin/LF10_Genome/versions/chromosomes/repeatMasker_20190215_hard/LF10g_chr.fa.out /home/CAM/qlin/CE10_Genome/versions/chromosomes/repeatMasker_20190215_hard/CE10g_chr.fa.out /home/CAM/qlin/parisii_Genome/versions/chromosomes/repeatMasker_20190215_hard/Mparg_chr.fa.out /home/CAM/qlin/MVBL_Genome/versions/chromosomes/repeatMasker_20190215_hard/MvBLg_chr.fa.out)

for s in ${!prefix[@]}
do
	awk '$3~/exon/{print $1,$4,$5}' OFS='\t' ${gtflocation[$s]} | sort -k1,1 -k2,2n > ${prefix[$s]}.exon.tmp
	bedtools merge -i ${prefix[$s]}.exon.tmp > ${prefix[$s]}.exon.bed
	rm ${prefix[$s]}.exon.tmp

	awk '$3~/intron/{print $1,$4,$5}' OFS='\t' ${gtflocation[$s]} | sort -k1,1 -k2,2n > ${prefix[$s]}.intron.tmp
	bedtools merge -i ${prefix[$s]}.intron.tmp > ${prefix[$s]}.intron.bed
	rm ${prefix[$s]}.intron.tmp

	awk '$3~/gene/{print $1,$4,$5}' OFS='\t' ${gtflocation[$s]} | sort -k1,1 -k2,2n > ${prefix[$s]}.gene.tmp
	bedtools merge -i ${prefix[$s]}.gene.tmp > ${prefix[$s]}.gene.bed
	rm ${prefix[$s]}.gene.tmp

	bedtools complement -i ${prefix[$s]}.gene.bed -g ${lenlocation[$s]} > ${prefix[$s]}.intergenic.bed

##################################################################################################################
# It turns out that MUMmer cannot detect any SNPs in the repeats regions. So I masked all repeats related codes. #
##################################################################################################################
#	tail -n +4 ${maskout[$s]} |awk '{print $5"\t"$6"\t"$7"\t"$10}'> ${prefix[$s]}.repeats.bed
#	
#	teclass=(MlrDNA CenR Tandem_Repeat Helitron hAT "MLE|Stowaway" MULE CACTA "Harbinger|Tourist" LINE SINE Copia Gypsy)
#	for te in ${teclass[@]}
#	do
#		perl -slane 'print if $F[3]=~/$te/' -- -te=$te ${prefix[$s]}.repeats.bed > ${prefix[$s]}.$te\.bed
#	done
done

# count main types by genomic features
#features=(exon intron gene intergenic MlrDNA CenR Tandem_Repeat Helitron hAT "MLE|Stowaway" MULE CACTA "Harbinger|Tourist" LINE SINE Copia Gypsy)
features=(exon intron intergenic)

fileName=counts_by_features_across_main_types.txt
perl -e 'print "type\tspecies\tfeature\tcount\n"' > $fileName
for s in ${!prefix[@]}
do
	for f in ${features[@]}
	do
		cat ${prefix[$s]}.specific.vcf | perl -lane '{$tmp=$F[2]."_".$F[3]; print "$F[0]\t$F[1]\t$F[1]" if $tmp =~ /^A_T$|^A_C$|^G_T$|^G_C$|^T_A$|^T_G$|^C_A$|^C_G$/}' > tmp.bed
		bedtools coverage -a ${prefix[$s]}.$f\.bed -b tmp.bed -counts | perl -slane '{$sum+=$F[3]}END{print "transversion\t$label\t$feature\t$sum"}' -- -label=${label[$s]} -feature=$f >> $fileName
		rm tmp.bed

		cat ${prefix[$s]}.specific.vcf | perl -lane '{$tmp=$F[2]."_".$F[3]; print "$F[0]\t$F[1]\t$F[1]" if $tmp =~ /^G_A$|^A_G$|^C_T$|^T_C$/}' > tmp.bed
		bedtools coverage -a ${prefix[$s]}.$f\.bed -b tmp.bed -counts | perl -slane '{$sum+=$F[3]}END{print "transition\t$label\t$feature\t$sum"}' -- -label=${label[$s]} -feature=$f >> $fileName
                rm tmp.bed

		cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]" if length($F[2])>length($F[3])' > tmp.bed
		bedtools coverage -a ${prefix[$s]}.$f\.bed -b tmp.bed -counts | perl -slane '{$sum+=$F[3]}END{print "insertion\t$label\t$feature\t$sum"}' -- -label=${label[$s]} -feature=$f >> $fileName
		rm tmp.bed

		cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]" if length($F[2])<length($F[3])' > tmp.bed
		bedtools coverage -a ${prefix[$s]}.$f\.bed -b tmp.bed -counts | perl -slane '{$sum+=$F[3]}END{print "deletion\t$label\t$feature\t$sum"}' -- -label=${label[$s]} -feature=$f >> $fileName
                rm tmp.bed
	done
done

# count indels of different length by genomic features
#features=(exon intron gene intergenic MlrDNA CenR Tandem_Repeat Helitron hAT "MLE|Stowaway" MULE CACTA "Harbinger|Tourist" LINE SINE Copia Gypsy)
fileName=counts_by_features_across_indel_length.txt
perl -e 'print "type\tlength\tspecies\tfeature\tcount\n"' > $fileName
for s in ${!prefix[@]}
do
	cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]\t",length($F[2])-length($F[3]) if length($F[2])>length($F[3])' > tmp.bed
	cut -f4 tmp.bed | sort -n |uniq > length.tmp
	len=()
	while read line; do len+=("$line"); done < length.tmp
	for l in ${len[@]}
	do
		cat tmp.bed | perl -slane 'if($F[3]==$len){print}' -- -len=$l > mytmp.bed
		for f in ${features[@]}
		do
			bedtools coverage -a ${prefix[$s]}.$f\.bed -b mytmp.bed -counts | perl -slane '{$sum+=$F[3]}END{print "insertion\t$len\t$label\t$feature\t$sum"}' -- -label=${label[$s]} -len=$l -feature=$f >> $fileName
		done
	done
	rm mytmp.bed length.tmp tmp.bed 
	
	cat ${prefix[$s]}.specific.vcf | perl -lane 'print "$F[0]\t$F[1]\t$F[1]\t",length($F[3])-length($F[2]) if length($F[2])<length($F[3])' > tmp.bed 
	cut -f4 tmp.bed | sort -n |uniq > length.tmp
        len=()  
        while read line; do len+=("$line"); done < length.tmp
        for l in ${len[@]}
        do  
		cat tmp.bed | perl -slane 'if($F[3]==$len){print}' -- -len=$l > mytmp.bed
                for f in ${features[@]}
                do
			bedtools coverage -a ${prefix[$s]}.$f\.bed -b mytmp.bed -counts | perl -slane '{$sum+=$F[3]}END{print "deletion\t$len\t$label\t$feature\t$sum"}' -- -label=${label[$s]} -len=$l -feature=$f >> $fileName
		done    
	done
	rm mytmp.bed length.tmp tmp.bed
done
awk '{if($1~/deletion/){print $1,$2,$3,$4,"-"$5}else{print}}' OFS='\t' $fileName > tmp
mv tmp $fileName 

rm *intergenic.bed *exon.bed *intron.bed *gene.bed


