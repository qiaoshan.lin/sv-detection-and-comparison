#!/bin/bash
#SBATCH --job-name=filter2
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bedtools

prefix=(ml mc mp mv)

file=(${prefix[$SLURM_ARRAY_TASK_ID]}.m?m?.one-to-one.bed)

cp ${file[0]} $SLURM_ARRAY_TASK_ID\.tmp
for f in ${file[@]}
do
	bedtools intersect -a $SLURM_ARRAY_TASK_ID\.tmp -b $f > ${prefix[$SLURM_ARRAY_TASK_ID]}.flt.bed
	mv ${prefix[$SLURM_ARRAY_TASK_ID]}.flt.bed $SLURM_ARRAY_TASK_ID\.tmp
done

sort -k1,1 -k2,2n $SLURM_ARRAY_TASK_ID\.tmp > ${prefix[$SLURM_ARRAY_TASK_ID]}.flt.bed

rm $SLURM_ARRAY_TASK_ID\.tmp 
rm ${prefix[$SLURM_ARRAY_TASK_ID]}.m?m?.one-to-one.bed


