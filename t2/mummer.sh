#!/bin/bash
#SBATCH --job-name=mummer
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load MUMmer/3.23
module load python

ml=/home/CAM/qlin/resource/LF10/LF10g_v2.0_hardmasking_nucleus.fa
mc=/home/CAM/qlin/resource/CE10/CE10g_v2.0_hardmasking_nucleus.fa
mp=/home/CAM/qlin/resource/Mpar/Mparg_v2.0_hardmasking_nucleus.fa
mv=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0_hardmasking_nucleus.fa

pair=("$ml $mc" "$ml $mp" "$ml $mv" "$mc $mp" "$mc $mv" "$mp $mv" "$mc $ml" "$mp $ml" "$mv $ml" "$mp $mc" "$mv $mc" "$mv $mp")
prefix=(mlmc mlmp mlmv mcmp mcmv mpmv mcml mpml mvml mpmc mvmc mvmp)

nucmer --mum -g 100 -p ${prefix[$SLURM_ARRAY_TASK_ID]} ${pair[$SLURM_ARRAY_TASK_ID]}
show-snps -ClrT -x 1 ${prefix[$SLURM_ARRAY_TASK_ID]}.delta > ${prefix[$SLURM_ARRAY_TASK_ID]}.snps

python3.4 MUMmerSNPs2VCF.py ${prefix[$SLURM_ARRAY_TASK_ID]}.snps ${prefix[$SLURM_ARRAY_TASK_ID]}.vcf  

show-coords -T -r -o ${prefix[$SLURM_ARRAY_TASK_ID]}.delta > ${prefix[$SLURM_ARRAY_TASK_ID]}.coords

sed -i '1,4d' ${prefix[$SLURM_ARRAY_TASK_ID]}.coords





