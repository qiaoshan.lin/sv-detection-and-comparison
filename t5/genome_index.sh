#!/bin/bash
#SBATCH --job-name=genome_index
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o genome_index_%A_%a.out
#SBATCH -e genome_index_%A_%a.err

export PATH=$PATH:~/local/apps/smartie-sv/bin/

mc=/home/CAM/qlin/CE10_Genome/versions/chromosomes/CE10g_chr.fa
ml=/home/CAM/qlin/LF10_Genome/versions/chromosomes/LF10g_chr.fa
mp=/home/CAM/qlin/parisii_Genome/versions/chromosomes/Mparg_chr.fa
mv=/home/CAM/qlin/MVBL_Genome/versions/chromosomes/MvBLg_chr.fa

# for calling species specific SV
ref=("$ml" "$mc" "$mp" "$mv")
sawriter ${ref[$SLURM_ARRAY_TASK_ID]}


