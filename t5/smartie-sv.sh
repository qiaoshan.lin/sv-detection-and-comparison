#!/bin/bash
#SBATCH --job-name=smartie-sv
#SBATCH -c 4
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o smartie-sv_%A_%a.out
#SBATCH -e smartie-sv_%A_%a.err

module load bedtools 
module load hdf5

export PATH=$PATH:~/local/apps/smartie-sv/bin/
export PATH=$PATH:~/miniconda3/bin/

mc_genome=/home/CAM/qlin/CE10_Genome/versions/chromosomes/CE10g_chr.fa
ml_genome=/home/CAM/qlin/LF10_Genome/versions/chromosomes/LF10g_chr.fa
mp_genome=/home/CAM/qlin/parisii_Genome/versions/chromosomes/Mparg_chr.fa
mv_genome=/home/CAM/qlin/MVBL_Genome/versions/chromosomes/MvBLg_chr.fa

ml_contig=/home/CAM/qlin/LF10_Genome/versions/1.9.3/LF10g_v1.93.fa
mc_contig=/home/CAM/qlin/CE10_Genome/versions/1.9.3/CE10g_v1.93.fa
mv_contig=/home/CAM/qlin/MVBL_Genome/versions/1.9.3/MvBLg_v1.93.fa
mp_contig=/home/CAM/qlin/parisii_Genome/versions/1.9.3/Mparg_v1.93.fa

ref=("$ml_genome" "$ml_genome" "$ml_genome" "$mc_genome" "$mc_genome" "$mc_genome" "$mp_genome" "$mp_genome" "$mp_genome" "$mv_genome" "$mv_genome" "$mv_genome")

query=("$mc_contig" "$mp_contig" "$mv_contig" "$ml_contig" "$mp_contig" "$mv_contig" "$ml_contig" "$mc_contig" "$mv_contig" "$ml_contig" "$mc_contig" "$mp_contig")

ref_label=("LF10" "LF10" "LF10" "CE10" "CE10" "CE10" "Mpar" "Mpar" "Mpar" "MvBL" "MvBL" "MvBL")

query_label=("CE10" "Mpar" "MvBL" "LF10" "Mpar" "MvBL" "LF10" "CE10" "MvBL" "LF10" "CE10" "Mpar")

sed "s|query_fasta|${query[$SLURM_ARRAY_TASK_ID]}|" config.json > $SLURM_ARRAY_TASK_ID\.config.json
sed -i "s|target_fasta|${ref[$SLURM_ARRAY_TASK_ID]}|" $SLURM_ARRAY_TASK_ID\.config.json
sed -i "s|query_label|${query_label[$SLURM_ARRAY_TASK_ID]}|" $SLURM_ARRAY_TASK_ID\.config.json
sed -i "s|target_label|${ref_label[$SLURM_ARRAY_TASK_ID]}|" $SLURM_ARRAY_TASK_ID\.config.json

sed "s|config.json|$SLURM_ARRAY_TASK_ID\.config.json|" Snakefile > Snakefile.$SLURM_ARRAY_TASK_ID
snakemake -p --cores 4 --verbose -s Snakefile.$SLURM_ARRAY_TASK_ID -w 30

rm $SLURM_ARRAY_TASK_ID\.config.json
rm Snakefile.$SLURM_ARRAY_TASK_ID


