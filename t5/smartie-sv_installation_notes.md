# smartie-sv installation notes

## Step 1: google search 'Miniconda Python3' and download the python 3.7 bash installer onto server

## Step 2: run the installer

```shell
sh Miniconda3-latest-Linux-x86_64.sh 
```

then follow the instruction on screen 

## Step 3: install packages and export the path
```shell
~/miniconda3/bin/conda install numpy
~/miniconda3/bin/conda install -c bioconda -c conda-forge snakemake

export PATH=$PATH:~/miniconda3/bin/
```
## Now you can run snakemake!
