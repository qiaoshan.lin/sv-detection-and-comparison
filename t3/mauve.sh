#!/bin/bash
#SBATCH --job-name=mauve
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o mauve_%j.out
#SBATCH -e mauve_%j.err

module load module load mauve/2015-02-13
 
mc=/home/CAM/qlin/CE10_Genome/versions/chromosomes/CE10g_chr.fa
ml=/home/CAM/qlin/LF10_Genome/versions/chromosomes/LF10g_chr.fa  
mp=/home/CAM/qlin/parisii_Genome/versions/chromosomes/Mparg_chr.fa
mv=/home/CAM/qlin/MVBL_Genome/versions/chromosomes/MvBLg_chr.fa

/isg/shared/apps/mauve/2015-02-13/linux-x64/progressiveMauve --output=mauve.xmfa $ml $mc $mp $mv

