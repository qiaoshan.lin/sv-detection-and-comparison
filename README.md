### Trial 1 Align the colinear regions from mcscan *```Dropped```*

>  Since the colinear regions are only defined by genes but not by sequence similarity, it would be insufficient for calling structure variants from these regions.

### Trial 2 Align between genomes by MUMmer3 and call SNPs

* [x] 1.  use MUMmer to call SNPs(<100bp)

>  Run [t2/mummer.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/mummer.sh) (~1 day). Remember to copy [t2/MUMmerSNPs2VCF.py](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/MUMmerSNPs2VCF.py) (from [liangjiaoxue/PythonNGSTools](https://github.com/liangjiaoxue/PythonNGSTools/blob/master/MUMmerSNPs2VCF.py)) into the same directory.

:exclamation: Note that the alignment was set to be unique in both the reference and query. So there is no capability to detect SNPs in repetitive regions.

:exclamation: Although I set the mummer parameter to find unique alignments in both reference and query, it still found repetitive alignments. So we need to filter the SNPs additionally so as to focus only on one-to-one-to-one-to-one regions.

>  Run [t2/filter1.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/filter1.sh) and [t2/filter2.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/filter2.sh).

##### **Do statistics for 4 cases:**

**Case 1. Two types of SNPs; one is specific to one species while the other one is shared by the other three species**

Try to find any region that evolved more rapidly in only one species, which might contribute to the species-specific traits. 

* [x] 2.  extract species specific SNPs 

>  Run [t2/case1pre.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1pre.sh) (5~15 minutes). 

* [x] 3.  make statistics tables of **basic counts**, then plot the data

>  See [t2/case1A.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1A.sh) (~2 minutes) and [t2/case1A.r](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1A.r).

* [x] 4.  make statistics tables of **counts by genomic windows**, then plot the data

>  See [t2/case1B.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1B.sh) (~5 minutes) and [t2/case1B.r](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1B.r). 

:exclamation: Note that the genomic regions aligned by mummer should be marked on plots of counts by windows across genomes. Because if there is a region having no alignment, it won't detect any SNPs there. So we need to mark the regions to show the capability of SNPs detection by MUMmer. But I don't know the criteria of alignment for MUMmer to call SNPs. So here I set a criteria that minimum alignment length = 10000 and minimum percent identity = 0.9 for creating the aligned regions.

:exclamation: At first, I set the criteria for show-coords as above. But the aligned region length was only about 8Mb, which is way lower than expected. So I remove the criteria and got aligned region length of about 30Mb, which is more reasonable.

* [x] 5.  make statistics tables of **counts by genomic features**, then plot the data

>  See [t2/case1C.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1C.sh) (15~30 minutes) and [t2/case1C.r](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case1C.r).

**Case 2. Two or three types of SNPs; one is shared by two species**

Try to find any region that is more conserved between two species. May provide some evidences for lineage sorting, etc.

* [x] 6.  extract species specific SNPs

>  Run [t2/case2pre1.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case2pre1.sh) and [t2/case2pre2.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t2/case2pre2.sh) (~8 minutes).

**Case 3. Four types of SNPs; nothing shared between species**

Try to find any region that differentiated very rapidly, which might have something to do with positive/relax selection.

### Trial 3 Try mauve *```Failed```*

>  It runs for a week on mac but never finish.

### Trial 4 Align between genomes by minimap2 and call large structure variances by sniffles *```Dropped```*
* [x] 1.  align between genomes by minimap2 and transform into sorted bam format with MD

>  It required too much resource to add MD, i.e. either failed or never start. 

### Trial 5 Call structure variances by smartie-sv
* [x] 1.  install smartie-sv

>  It's important to install snakemake before installing smartie-sv. See how to install in [t5/smartie-sv_installation_notes.md](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t5/smartie-sv_installation_notes.md).

* [x] 2.  build genome index 

>  See [t5/genome_index.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t5/genome_index.sh).
* [x] 3.  align contigs from one genome onto another genome

>  See [t5/smartie-sv.sh](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t5/smartie-sv.sh). Remember to copy [t5/Snakefile](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t5/Snakefile) and [t5/config.json](https://gitlab.com/qiaoshan.lin/sv-detection-and-comparison/blob/master/t5/config.json) into the same directory. Edit them if necessary. 
### Trial 6 Align between genomes by NGMLR and call large structure variances by sniffles *```Dropped```*

>  Since trial5 works.

### Trial 7 Align between genomes by MUMmer4 and call SNPs
