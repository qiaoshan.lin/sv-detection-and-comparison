#!/bin/bash
#SBATCH --job-name=minimap2
#SBATCH -c 12
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0,2,3,5,8,9,11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o minimap2_%A_%a.out
#SBATCH -e minimap2_%A_%a.err

module load minimap2/2.17
module load samtools

mc=/home/CAM/qlin/CE10_Genome/versions/chromosomes/CE10g_chr.fa
ml=/home/CAM/qlin/LF10_Genome/versions/chromosomes/LF10g_chr.fa  
mp=/home/CAM/qlin/parisii_Genome/versions/chromosomes/Mparg_chr.fa
mv=/home/CAM/qlin/MVBL_Genome/versions/chromosomes/MvBLg_chr.fa

pair=("$ml $mc" "$ml $mp" "$ml $mv" "$mc $ml" "$mc $mp" "$mc $mv" "$mp $ml" "$mp $mc" "$mp $mv" "$mv $ml" "$mv $mc" "$mv $mp")
prefix=(mlmc mlmp mlmv mcml mcmp mcmv mpml mpmc mpmv mvml mvmc mvmp)

#minimap2 -k19 -w19 -A1 -B9 -O16,41 -E2,1 -s200 -z200 -N50 --min-occ-floor=100 -a -t 4 ${pair[$SLURM_ARRAY_TASK_ID]} > ${prefix[$SLURM_ARRAY_TASK_ID]}.sam

ref=("$ml" "$ml" "$ml" "$mc" "$mc" "$mc" "$mp" "$mp" "$mp" "$mv" "$mv" "$mv")

#samtools view -bST ${ref[$SLURM_ARRAY_TASK_ID]} -@ 8 ${prefix[$SLURM_ARRAY_TASK_ID]}.sam > ${prefix[$SLURM_ARRAY_TASK_ID]}.bam
#samtools sort -@ 8 -o ${prefix[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${prefix[$SLURM_ARRAY_TASK_ID]}tmp -O bam -@ 8 ${prefix[$SLURM_ARRAY_TASK_ID]}.bam
#rm ${prefix[$SLURM_ARRAY_TASK_ID]}.sam ${prefix[$SLURM_ARRAY_TASK_ID]}.bam
#samtools index ${prefix[$SLURM_ARRAY_TASK_ID]}.sort.bam
samtools calmd --output-fmt-option nthreads=12 ${prefix[$SLURM_ARRAY_TASK_ID]}.sort.bam ${ref[$SLURM_ARRAY_TASK_ID]} > ${prefix[$SLURM_ARRAY_TASK_ID]}.sort.calmd.bam

