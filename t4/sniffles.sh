#!/bin/bash
#SBATCH --job-name=sniffles
#SBATCH -c 4
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o sniffles_%A_%a.out
#SBATCH -e sniffles_%A_%a.err

prefix=(mlmc mlmp mlmv mcml mcmp mcmv mpml mpmc mpmv mvml mvmc mvmp)

export PATH=$PATH:~/local/Sniffles-master/bin/sniffles-core-1.0.11/

sniffles -s 1 --max_num_splits 20 -t 4 -m ${prefix[$SLURM_ARRAY_TASK_ID]}.sort.calmd.bam -v ${prefix[$SLURM_ARRAY_TASK_ID]}.vcf

